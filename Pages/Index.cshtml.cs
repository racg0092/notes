﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using notes.data;

namespace notes.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly INotesData notesData;
        
        public List<string> notes { get; set; }
        public IndexModel(ILogger<IndexModel> logger, INotesData notesData)
        {
            this.notesData = notesData;
            _logger = logger;
        }

        public void OnGet()
        {
            notes = this.notesData.GetAll().ToList();
        }
    }
}
